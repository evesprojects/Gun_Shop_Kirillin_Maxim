USE [ShopGun]
GO
/****** Object:  Table [dbo].[Category]    Script Date: 29.09.2023 2:25:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[IDCategory] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NOT NULL,
	[IDCertificate] [int] NULL,
 CONSTRAINT [PK_Category] PRIMARY KEY CLUSTERED 
(
	[IDCategory] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Certificate]    Script Date: 29.09.2023 2:25:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Certificate](
	[IDCertificate] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](500) NULL,
 CONSTRAINT [PK_Certificate] PRIMARY KEY CLUSTERED 
(
	[IDCertificate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CertificateClient]    Script Date: 29.09.2023 2:25:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CertificateClient](
	[IDCertificateClient] [int] IDENTITY(1,1) NOT NULL,
	[IDCertificate] [int] NOT NULL,
	[IDClient] [int] NOT NULL,
 CONSTRAINT [PK_CertificateClient] PRIMARY KEY CLUSTERED 
(
	[IDCertificateClient] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Check]    Script Date: 29.09.2023 2:25:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Check](
	[IDCheck] [int] IDENTITY(1,1) NOT NULL,
	[IDProductList] [int] NOT NULL,
	[IDEmployee] [int] NOT NULL,
	[DateOfSale] [datetime] NOT NULL,
 CONSTRAINT [PK_Check] PRIMARY KEY CLUSTERED 
(
	[IDCheck] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Client]    Script Date: 29.09.2023 2:25:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Client](
	[IDClient] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[Patronymic] [nvarchar](100) NOT NULL,
	[Birthday] [date] NOT NULL,
	[Phone] [char](11) NOT NULL,
	[IDGender] [int] NOT NULL,
	[PasportSeries] [char](4) NOT NULL,
	[PasportNumber] [char](6) NOT NULL,
	[IDLogin] [int] NULL,
 CONSTRAINT [PK_Client] PRIMARY KEY CLUSTERED 
(
	[IDClient] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Emloyee]    Script Date: 29.09.2023 2:25:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Emloyee](
	[IDEmployee] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [nvarchar](100) NOT NULL,
	[LastName] [nvarchar](100) NOT NULL,
	[Patronymic] [nvarchar](100) NOT NULL,
	[Phone] [char](11) NOT NULL,
	[IDGender] [int] NOT NULL,
	[IDPost] [int] NOT NULL,
	[IDLogin] [int] NULL,
 CONSTRAINT [PK_Emloyee] PRIMARY KEY CLUSTERED 
(
	[IDEmployee] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmloyeeWorkShift]    Script Date: 29.09.2023 2:25:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmloyeeWorkShift](
	[IDEmployerWorkShift] [int] IDENTITY(1,1) NOT NULL,
	[IDEmployee] [int] NOT NULL,
	[IDWorkshift] [int] NOT NULL,
 CONSTRAINT [PK_EmloyerWorkShift] PRIMARY KEY CLUSTERED 
(
	[IDEmployerWorkShift] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Gender]    Script Date: 29.09.2023 2:25:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Gender](
	[IDGender] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_Gender] PRIMARY KEY CLUSTERED 
(
	[IDGender] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Login]    Script Date: 29.09.2023 2:25:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Login](
	[IDLogin] [int] IDENTITY(1,1) NOT NULL,
	[Login] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Login] PRIMARY KEY CLUSTERED 
(
	[IDLogin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Post]    Script Date: 29.09.2023 2:25:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Post](
	[IDPost] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK_Post] PRIMARY KEY CLUSTERED 
(
	[IDPost] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 29.09.2023 2:25:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[IDProduct] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NOT NULL,
	[Price] [decimal](10, 2) NOT NULL,
	[IDCategory] [int] NOT NULL,
	[Description] [nvarchar](500) NULL,
 CONSTRAINT [PK_Product] PRIMARY KEY CLUSTERED 
(
	[IDProduct] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ProductList]    Script Date: 29.09.2023 2:25:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ProductList](
	[IDProductList] [int] IDENTITY(1,1) NOT NULL,
	[IDProduct] [int] NOT NULL,
	[QuantityProd] [int] NOT NULL,
	[IDClient] [int] NOT NULL,
 CONSTRAINT [PK_ProductList] PRIMARY KEY CLUSTERED 
(
	[IDProductList] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[WorkShift]    Script Date: 29.09.2023 2:25:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[WorkShift](
	[IDWorkShift] [int] IDENTITY(1,1) NOT NULL,
	[StartTime] [datetime] NOT NULL,
	[EndTime] [datetime] NOT NULL,
 CONSTRAINT [PK_WorkShift] PRIMARY KEY CLUSTERED 
(
	[IDWorkShift] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

INSERT [dbo].[Category] ([IDCategory], [Title], [IDCertificate]) VALUES (1, N'Без категории', NULL)
INSERT [dbo].[Category] ([IDCategory], [Title], [IDCertificate]) VALUES (2, N'Холодное оружее', NULL)
INSERT [dbo].[Category] ([IDCategory], [Title], [IDCertificate]) VALUES (3, N'Гладкоствольное оружие', 4)
INSERT [dbo].[Category] ([IDCategory], [Title], [IDCertificate]) VALUES (4, N'Нарезное оружие', 5)
INSERT [dbo].[Category] ([IDCategory], [Title], [IDCertificate]) VALUES (5, N'Бронекостюм', NULL)
INSERT [dbo].[Category] ([IDCategory], [Title], [IDCertificate]) VALUES (6, N'Одежда', NULL)
INSERT [dbo].[Category] ([IDCategory], [Title], [IDCertificate]) VALUES (7, N'Патроны', NULL)
INSERT [dbo].[Category] ([IDCategory], [Title], [IDCertificate]) VALUES (8, N'Травматическое оружие', 2)
INSERT [dbo].[Category] ([IDCategory], [Title], [IDCertificate]) VALUES (9, N'Газовое оружие', 1)
INSERT [dbo].[Category] ([IDCategory], [Title], [IDCertificate]) VALUES (10, N'Пневматическое оружие', 3)
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Certificate] ON 

INSERT [dbo].[Certificate] ([IDCertificate], [Title], [Description]) VALUES (1, N'ЛОа', N'лицензия на газовое оружие')
INSERT [dbo].[Certificate] ([IDCertificate], [Title], [Description]) VALUES (2, N'ЛОПа', N'лицензия на травматическое оружие (ОООП);')
INSERT [dbo].[Certificate] ([IDCertificate], [Title], [Description]) VALUES (3, N'ЛПа', N'лицензия на пневматическое оружие с мощностью свыше 7,5 Дж')
INSERT [dbo].[Certificate] ([IDCertificate], [Title], [Description]) VALUES (4, N'ЛГа', N'лицензия на гладкоствольное оружие')
INSERT [dbo].[Certificate] ([IDCertificate], [Title], [Description]) VALUES (5, N'ЛНа', N'лицензия на нарезное оружие')
SET IDENTITY_INSERT [dbo].[Certificate] OFF
GO
SET IDENTITY_INSERT [dbo].[CertificateClient] ON 

INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (1, 2, 23)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (2, 1, 48)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (3, 4, 16)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (4, 2, 13)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (5, 3, 47)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (6, 1, 43)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (7, 1, 29)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (8, 3, 32)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (9, 3, 9)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (10, 3, 49)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (11, 1, 17)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (12, 2, 40)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (13, 2, 47)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (14, 4, 29)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (15, 1, 20)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (16, 4, 12)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (17, 3, 39)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (18, 3, 35)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (19, 2, 4)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (20, 4, 10)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (21, 5, 32)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (22, 1, 8)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (23, 4, 16)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (24, 4, 31)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (25, 3, 23)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (26, 4, 11)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (27, 5, 28)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (28, 5, 43)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (29, 2, 7)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (30, 1, 5)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (31, 3, 3)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (32, 2, 34)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (33, 1, 13)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (34, 5, 6)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (35, 3, 34)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (36, 1, 2)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (37, 2, 38)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (38, 2, 30)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (39, 4, 25)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (40, 5, 41)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (41, 2, 4)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (42, 2, 20)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (43, 2, 12)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (44, 4, 37)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (45, 5, 11)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (46, 4, 32)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (47, 2, 39)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (48, 4, 30)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (49, 5, 3)
INSERT [dbo].[CertificateClient] ([IDCertificateClient], [IDCertificate], [IDClient]) VALUES (50, 5, 33)
SET IDENTITY_INSERT [dbo].[CertificateClient] OFF
GO
SET IDENTITY_INSERT [dbo].[Check] ON 

INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (1, 33, 20, CAST(N'2022-05-06T17:22:07.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (2, 10, 17, CAST(N'2023-02-12T08:23:42.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (3, 3, 19, CAST(N'2022-11-21T00:42:21.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (4, 17, 50, CAST(N'2023-04-23T21:25:42.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (5, 24, 47, CAST(N'2022-04-05T14:51:31.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (6, 29, 23, CAST(N'2023-09-08T18:23:11.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (7, 5, 20, CAST(N'2023-02-16T02:53:52.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (8, 10, 28, CAST(N'2022-09-18T22:21:51.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (9, 45, 21, CAST(N'2022-09-19T05:01:35.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (10, 35, 42, CAST(N'2022-07-20T10:53:27.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (11, 39, 3, CAST(N'2023-07-04T14:47:54.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (12, 28, 5, CAST(N'2022-09-26T22:28:37.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (13, 31, 35, CAST(N'2023-06-06T14:48:45.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (14, 49, 16, CAST(N'2023-05-14T05:45:38.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (15, 7, 45, CAST(N'2023-11-16T08:30:07.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (16, 28, 8, CAST(N'2023-05-26T18:02:44.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (17, 49, 7, CAST(N'2023-01-26T14:18:43.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (18, 4, 10, CAST(N'2023-07-27T18:06:03.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (19, 16, 21, CAST(N'2022-08-11T03:25:45.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (20, 2, 23, CAST(N'2022-06-14T10:21:46.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (21, 37, 23, CAST(N'2023-04-06T02:21:12.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (22, 15, 21, CAST(N'2022-10-18T12:26:24.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (23, 21, 21, CAST(N'2022-04-05T16:03:39.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (24, 8, 4, CAST(N'2022-08-25T06:59:45.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (25, 10, 38, CAST(N'2023-09-28T22:48:15.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (26, 37, 15, CAST(N'2022-11-13T20:48:43.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (27, 45, 11, CAST(N'2022-11-28T21:43:09.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (28, 23, 47, CAST(N'2023-05-10T19:21:20.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (29, 25, 21, CAST(N'2023-12-19T20:37:14.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (30, 22, 27, CAST(N'2023-03-24T22:19:06.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (31, 20, 13, CAST(N'2023-06-07T08:42:42.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (32, 29, 20, CAST(N'2022-11-11T21:14:16.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (33, 18, 1, CAST(N'2022-09-10T11:57:26.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (34, 47, 31, CAST(N'2022-11-28T21:02:14.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (35, 35, 20, CAST(N'2023-07-21T21:15:58.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (36, 19, 18, CAST(N'2023-08-24T10:19:59.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (37, 26, 32, CAST(N'2023-12-05T14:50:01.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (38, 24, 42, CAST(N'2022-06-21T14:06:25.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (39, 22, 34, CAST(N'2022-04-16T21:42:57.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (40, 40, 43, CAST(N'2023-11-18T01:56:05.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (41, 34, 3, CAST(N'2022-04-13T13:20:49.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (42, 6, 36, CAST(N'2023-02-14T01:01:27.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (43, 28, 25, CAST(N'2023-10-02T02:54:04.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (44, 43, 28, CAST(N'2023-05-01T03:57:51.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (45, 24, 41, CAST(N'2023-06-18T05:35:28.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (46, 1, 2, CAST(N'2023-12-14T17:33:54.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (47, 11, 19, CAST(N'2022-11-18T03:42:49.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (48, 18, 27, CAST(N'2022-05-25T14:00:27.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (49, 48, 32, CAST(N'2022-12-15T11:52:38.000' AS DateTime))
INSERT [dbo].[Check] ([IDCheck], [IDProductList], [IDEmployee], [DateOfSale]) VALUES (50, 15, 39, CAST(N'2023-01-22T15:05:05.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[Check] OFF
GO
SET IDENTITY_INSERT [dbo].[Client] ON 

INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (1, N'Нефедова', N'Ирина', N'Дмитриевна', CAST(N'1993-11-04' AS Date), N'89079802123', 1, N'8609', N'760695', 3)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (2, N'Королев', N'Тимофей', N'Никитич', CAST(N'1976-05-08' AS Date), N'89092633075', 2, N'2122', N'824024', 52)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (3, N'Акимов', N'Александр', N'Алексеевич', CAST(N'1967-12-26' AS Date), N'89768550475', 2, N'7707', N'742286', 53)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (4, N'Мешкова', N'Каролина', N'Николаевна', CAST(N'2000-09-23' AS Date), N'89104345248', 2, N'4200', N'230457', 54)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (5, N'Миронова', N'Ксения', N'Юрьевна', CAST(N'1975-03-22' AS Date), N'89567453550', 1, N'3814', N'647916', 55)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (6, N'Власов', N'Марк', N'Фёдорович', CAST(N'1989-04-20' AS Date), N'89077794542', 2, N'3721', N'454174', 56)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (7, N'Минина', N'Евгения', N'Александровна', CAST(N'1992-07-04' AS Date), N'89214292177', 2, N'5383', N'649782', 57)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (8, N'Михеева', N'Виктория', N'Егоровна', CAST(N'1976-08-22' AS Date), N'89914466125', 1, N'5968', N'630316', 58)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (9, N'Зуева', N'Алиса', N'Константиновна', CAST(N'2002-11-28' AS Date), N'89827797426', 1, N'5879', N'439874', 59)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (10, N'Антонов', N'Даниил', N'Дмитриевич', CAST(N'1987-06-03' AS Date), N'89937962598', 2, N'6190', N'479800', 60)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (11, N'Медведев', N'Алексей', N'Викторович', CAST(N'1979-07-12' AS Date), N'89083296346', 1, N'6321', N'539009', 61)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (12, N'Королева', N'Ева', N'Платоновна', CAST(N'1962-02-01' AS Date), N'89776085671', 1, N'2211', N'654614', 62)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (13, N'Лапина', N'София', N'Павловна', CAST(N'1964-01-07' AS Date), N'89591891229', 1, N'3958', N'825317', 63)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (14, N'Рудаков', N'Артём', N'Тимурович', CAST(N'2000-07-27' AS Date), N'89075383763', 1, N'1823', N'526973', 64)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (15, N'Петровская', N'Ксения', N'Алексеевна', CAST(N'1985-03-17' AS Date), N'89110704548', 1, N'7449', N'828080', 65)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (16, N'Коровин', N'Арсений', N'Игоревич', CAST(N'1967-10-26' AS Date), N'89098288066', 2, N'4975', N'146549', 66)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (17, N'Давыдов', N'Владимир', N'Георгиевич', CAST(N'2005-01-10' AS Date), N'89397879241', 1, N'3179', N'441820', 67)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (18, N'Черных', N'Владислав', N'Даниилович', CAST(N'1962-10-27' AS Date), N'89740828003', 2, N'2102', N'280766', 68)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (19, N'Киселев', N'Роман', N'Захарович', CAST(N'1968-08-07' AS Date), N'89298011795', 2, N'6280', N'715890', 69)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (20, N'Маслов', N'Григорий', N'Михайлович', CAST(N'1996-01-15' AS Date), N'89013923467', 2, N'9974', N'584151', 70)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (21, N'Титов', N'Михаил', N'Георгиевич', CAST(N'1963-02-24' AS Date), N'89396798873', 1, N'3207', N'122485', 71)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (22, N'Колесов', N'Константин', N'Данисович', CAST(N'1961-01-04' AS Date), N'89602314024', 1, N'7804', N'962384', 72)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (23, N'Черкасов', N'Дмитрий', N'Алексеевич', CAST(N'2003-01-25' AS Date), N'89098662705', 1, N'9035', N'607125', 73)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (24, N'Осипов', N'Михаил', N'Андреевич', CAST(N'1976-05-07' AS Date), N'89886002183', 2, N'9032', N'505996', 74)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (25, N'Попова', N'Лейла', N'Сергеевна', CAST(N'1977-11-18' AS Date), N'89914678733', 2, N'8898', N'304908', 75)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (26, N'Смирнов', N'Владимир', N'Матвеевич', CAST(N'1981-08-13' AS Date), N'89056268603', 2, N'1686', N'207290', 76)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (27, N'Князев', N'Максим', N'Всеволодович', CAST(N'1984-12-08' AS Date), N'89646807724', 1, N'2248', N'438071', 77)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (28, N'Кравцов', N'Роман', N'Миронович', CAST(N'1963-03-06' AS Date), N'89969305285', 1, N'8735', N'783557', 78)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (29, N'Матвеев', N'Александр', N'Платонович', CAST(N'2000-01-14' AS Date), N'89614029798', 1, N'5332', N'407656', 79)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (30, N'Верещагина', N'Варвара', N'Александровна', CAST(N'1981-10-01' AS Date), N'89499158367', 1, N'5627', N'692141', 80)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (31, N'Борисов', N'Арсений', N'Мирославович', CAST(N'1996-05-25' AS Date), N'89678848073', 2, N'3890', N'834053', 81)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (32, N'Козлова', N'Виктория', N'Ибрагимовна', CAST(N'1968-11-02' AS Date), N'89156290728', 1, N'9139', N'795552', 82)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (33, N'Кузнецова', N'Мадина', N'Артёмовна', CAST(N'1989-11-03' AS Date), N'89669519718', 2, N'5310', N'562925', 83)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (34, N'Наумов', N'Алексей', N'Тимурович', CAST(N'1993-01-03' AS Date), N'89213178819', 2, N'8573', N'809698', 84)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (35, N'Бородина', N'Дарья', N'Егоровна', CAST(N'1965-11-04' AS Date), N'89888799145', 1, N'6718', N'829612', 85)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (36, N'Серебрякова', N'Ульяна', N'Матвеевна', CAST(N'1988-07-16' AS Date), N'89970160884', 1, N'1426', N'335294', 86)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (37, N'Иванова', N'София', N'Платоновна', CAST(N'1975-11-18' AS Date), N'89678035582', 2, N'2505', N'620602', 87)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (38, N'Васильева', N'Александра', N'Тихоновна', CAST(N'1968-06-03' AS Date), N'89041383275', 1, N'4704', N'712915', 88)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (39, N'Прокофьева', N'Есения', N'Максимовна', CAST(N'1988-06-11' AS Date), N'89993354198', 1, N'2265', N'390745', 89)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (40, N'Воробьева', N'Есения', N'Глебовна', CAST(N'1997-08-23' AS Date), N'89138668010', 2, N'5441', N'173451', 90)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (41, N'Иванов', N'Арсений', N'Артёмович', CAST(N'1969-02-20' AS Date), N'89567922786', 2, N'9228', N'197815', 91)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (42, N'Фадеева', N'Полина', N'Степановна', CAST(N'2000-04-21' AS Date), N'89506691671', 2, N'2154', N'523891', 92)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (43, N'Козлов', N'Андрей', N'Андреевич', CAST(N'1969-09-28' AS Date), N'89267931310', 1, N'4928', N'540285', 93)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (44, N'Полякова', N'Дарья', N'Петровна', CAST(N'1991-09-09' AS Date), N'89943224823', 1, N'6523', N'516056', 94)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (45, N'Виноградов', N'Кирилл', N'Фёдорович', CAST(N'1983-03-21' AS Date), N'89984851628', 1, N'5633', N'543461', 95)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (46, N'Савельева', N'Милана', N'Всеволодовна', CAST(N'1966-03-08' AS Date), N'89400476265', 1, N'3412', N'823087', 96)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (47, N'Голубев', N'Арсений', N'Максимович', CAST(N'1982-07-16' AS Date), N'89186685870', 1, N'7525', N'454776', 97)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (48, N'Потапова', N'Алиса', N'Павловна', CAST(N'1991-03-13' AS Date), N'89995985198', 1, N'6466', N'728805', 98)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (49, N'Седов', N'Семён', N'Артёмович', CAST(N'1983-04-13' AS Date), N'89154807679', 1, N'4748', N'164667', 99)
INSERT [dbo].[Client] ([IDClient], [FirstName], [LastName], [Patronymic], [Birthday], [Phone], [IDGender], [PasportSeries], [PasportNumber], [IDLogin]) VALUES (50, N'Прохоров', N'Николай', N'Ильич', CAST(N'2005-07-21' AS Date), N'89365453375', 1, N'8035', N'540110', 100)
SET IDENTITY_INSERT [dbo].[Client] OFF
GO
SET IDENTITY_INSERT [dbo].[Emloyee] ON 

INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (1, N'1', N'1', N'1', N'89027677389', 1, 1, 1)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (2, N'2', N'2', N'2', N'89716665332', 2, 2, 2)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (3, N'Ткачева', N'Марьяна', N'Ивановна', N'89822098578', 1, 2, 3)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (4, N'Прохорова', N'Екатерина', N'Тимуровна', N'89956365272', 1, 2, 4)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (5, N'Трошина', N'Анна', N'Артуровна', N'89580988878', 2, 2, 5)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (6, N'Беспалов', N'Артём', N'Павлович', N'89571534678', 1, 2, 6)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (7, N'Тарасова', N'Амина', N'Тимуровна', N'89813619678', 2, 2, 7)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (8, N'Голубева', N'Виктория', N'Арсентьевна', N'89614350167', 2, 2, 8)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (9, N'Глухова', N'Майя', N'Артёмовна', N'89587383053', 1, 2, 9)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (10, N'Трофимова', N'Ксения', N'Максимовна', N'89509934155', 1, 2, 10)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (11, N'Мешкова', N'Таисия', N'Петровна', N'89187151224', 1, 2, 11)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (12, N'Борисов', N'Пётр', N'Ильич', N'89534171512', 1, 2, 12)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (13, N'Васильева', N'Алина', N'Артемьевна', N'89828033138', 1, 2, 13)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (14, N'Попов', N'Егор', N'Никитич', N'89474575741', 1, 2, 14)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (15, N'Антонова', N'София', N'Михайловна', N'89003656690', 2, 2, 15)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (16, N'Ерофеева', N'Анастасия', N'Серафимовна', N'89413588679', 1, 2, 16)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (17, N'Васильев', N'Али', N'Фёдорович', N'89580828220', 2, 2, 17)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (18, N'Трофимова', N'Ксения', N'Всеволодовна', N'89563017771', 1, 2, 18)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (19, N'Морозов', N'Марк', N'Русланович', N'89607579082', 2, 2, 19)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (20, N'Макаров', N'Савелий', N'Святославович', N'89449834493', 2, 2, 20)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (21, N'Харитонов', N'Степан', N'Романович', N'89901417147', 2, 2, 21)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (22, N'Ермакова', N'Варвара', N'Михайловна', N'89736128909', 2, 2, 22)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (23, N'Овчинникова', N'София', N'Алексеевна', N'89065268737', 2, 2, 23)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (24, N'Богданов', N'Матвей', N'Арсентьевич', N'89480152582', 2, 2, 24)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (25, N'Федорова', N'Анна', N'Марковна', N'89679783546', 2, 2, 25)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (26, N'Шувалов', N'Арсений', N'Романович', N'89335188836', 1, 2, 26)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (27, N'Петровская', N'Агния', N'Платоновна', N'89082481558', 1, 2, 27)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (28, N'Быкова', N'Ксения', N'Григорьевна', N'89303299510', 2, 2, 28)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (29, N'Завьялова', N'Ульяна', N'Романовна', N'89556233414', 1, 2, 29)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (30, N'Иванова', N'Виктория', N'Глебовна', N'89868677595', 2, 2, 30)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (31, N'Москвин', N'Александр', N'Платонович', N'89620132544', 2, 2, 31)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (32, N'Петухова', N'Алиса', N'Михайловна', N'89367286585', 1, 2, 32)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (33, N'Яковлев', N'Иван', N'Андреевич', N'89467363573', 2, 2, 33)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (34, N'Корнилов', N'Никита', N'Львович', N'89049439385', 1, 2, 34)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (35, N'Михеев', N'Назар', N'Михайлович', N'89538316870', 1, 2, 35)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (36, N'Михайлов', N'Макар', N'Максимович', N'89677134909', 2, 2, 36)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (37, N'Чернов', N'Роман', N'Александрович', N'89643396142', 2, 2, 37)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (38, N'Медведев', N'Тимофей', N'Ильич', N'89706375738', 1, 2, 38)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (39, N'Горбунов', N'Данила', N'Никитич', N'89300247656', 2, 2, 39)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (40, N'Иванова', N'Милана', N'Данииловна', N'89818765247', 1, 2, 40)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (41, N'Кондратьева', N'Таисия', N'Максимовна', N'89240931862', 1, 2, 41)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (42, N'Титов', N'Владислав', N'Матвеевич', N'89453487697', 1, 2, 42)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (43, N'Богомолова', N'Сафия', N'Егоровна', N'89582919466', 2, 2, 43)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (44, N'Антипова', N'Милана', N'Александровна', N'89632980984', 1, 2, 44)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (45, N'Богданова', N'Наталья', N'Артуровна', N'89305535141', 1, 2, 45)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (46, N'Сазонова', N'Арина', N'Арсентьевна', N'89372272781', 1, 2, 46)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (47, N'Матвеева', N'Софья', N'Матвеевна', N'89203650279', 2, 2, 47)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (48, N'Евсеев', N'Тимофей', N'Иванович', N'89197353331', 2, 2, 48)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (49, N'Кузнецова', N'Мирослава', N'Андреевна', N'89657528776', 1, 2, 49)
INSERT [dbo].[Emloyee] ([IDEmployee], [FirstName], [LastName], [Patronymic], [Phone], [IDGender], [IDPost], [IDLogin]) VALUES (50, N'Волков', N'Михаил', N'Даниилович', N'89539411777', 1, 2, 50)
SET IDENTITY_INSERT [dbo].[Emloyee] OFF
GO
SET IDENTITY_INSERT [dbo].[EmloyeeWorkShift] ON 

INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (1, 13, 48)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (2, 15, 21)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (3, 22, 32)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (4, 19, 44)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (5, 26, 40)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (6, 8, 46)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (7, 25, 16)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (8, 22, 9)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (9, 28, 35)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (10, 25, 46)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (11, 19, 47)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (12, 22, 17)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (13, 1, 24)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (14, 17, 47)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (15, 11, 4)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (16, 38, 21)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (17, 21, 31)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (18, 4, 34)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (19, 44, 45)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (20, 43, 27)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (21, 36, 38)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (22, 32, 30)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (23, 36, 43)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (24, 49, 19)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (25, 15, 18)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (26, 8, 40)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (27, 40, 42)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (28, 8, 15)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (29, 37, 43)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (30, 28, 2)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (31, 14, 37)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (32, 31, 8)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (33, 48, 15)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (34, 28, 29)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (35, 3, 46)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (36, 12, 46)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (37, 36, 14)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (38, 43, 49)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (39, 15, 41)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (40, 7, 46)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (41, 45, 1)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (42, 5, 1)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (43, 21, 5)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (44, 1, 45)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (45, 34, 41)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (46, 21, 50)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (47, 41, 15)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (48, 17, 10)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (49, 41, 49)
INSERT [dbo].[EmloyeeWorkShift] ([IDEmployerWorkShift], [IDEmployee], [IDWorkshift]) VALUES (50, 28, 1)
SET IDENTITY_INSERT [dbo].[EmloyeeWorkShift] OFF
GO
SET IDENTITY_INSERT [dbo].[Gender] ON 

INSERT [dbo].[Gender] ([IDGender], [Title]) VALUES (1, N'Мужской')
INSERT [dbo].[Gender] ([IDGender], [Title]) VALUES (2, N'Женский')
SET IDENTITY_INSERT [dbo].[Gender] OFF
GO
SET IDENTITY_INSERT [dbo].[Login] ON 

INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (1, N'1', N'1')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (2, N'2', N'2')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (3, N'Wayne', N'Holloway357')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (4, N'Margaret', N'Williams51')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (5, N'Anthony', N'Baker777')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (6, N'Cynthia', N'Lindsey562')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (7, N'Patricia', N'Cruz281')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (8, N'Mildred', N'Rogers480')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (9, N'Dawn', N'Logan563')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (10, N'Charle', N'Anderson880')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (11, N'Michelle', N'Hayes23')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (12, N'Debras', N'Sutton227')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (13, N'Michaelot', N'Morris373')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (14, N'Jame', N'Smith501')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (15, N'Barbara', N'Thompson311')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (16, N'Matthew', N'Turner49')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (17, N'Donna', N'Edwards972')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (18, N'Nicholas', N'Gray627')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (19, N'Charlene', N'Duncan322')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (20, N'Ann', N'Thompson270')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (21, N'Janet', N'Cook210')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (22, N'Willie', N'Jones329')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (23, N'Daniel', N'Collins480')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (24, N'Roberto', N'Haynes697')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (25, N'Gary', N'Walker210')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (26, N'Bonnie', N'Williams81')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (27, N'Jeremy', N'Fowler457')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (28, N'William', N'Thomas585')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (29, N'Michael', N'Lewis359')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (30, N'Lisa', N'Osborne143')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (31, N'Kimberly', N'Adams141')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (32, N'Nathaniel', N'Brock429')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (33, N'Ralphd', N'Smith248')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (34, N'John', N'White366')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (35, N'Yolanda', N'Aguilar952')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (36, N'Christine', N'Wilson34')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (37, N'Lloyd', N'Walters485')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (38, N'Melissa', N'Berry830')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (39, N'Frank', N'Thomas559')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (40, N'Florence', N'Alvarez580')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (41, N'Helen', N'Barker261')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (42, N'Judy', N'Riley584')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (43, N'Tracey', N'Elliott989')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (44, N'Eric', N'Martin569')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (45, N'George', N'Lewis297')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (46, N'Bernard', N'Sanchez817')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (47, N'Jean', N'Morgan809')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (48, N'Glenn', N'Davis372')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (49, N'Richard', N'Casey433')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (50, N'Ethel', N'Luna674')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (51, N'3', N'3')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (52, N'Joe', N'Perry80')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (53, N'Catherine', N'Brown729')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (54, N'Douglas', N'Johnson247')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (55, N'Edward', N'Barton663')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (56, N'Brandon', N'Diaz644')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (57, N'Larry', N'Smith913')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (58, N'Jamek', N'Gilbert842')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (59, N'Michaeli', N'Garrett251')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (60, N'David', N'Diaz780')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (61, N'Kathy', N'Martinez642')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (62, N'Gertrude', N'Perry836')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (63, N'Thomas', N'Gomez574')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (64, N'Diana', N'Davis46')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (65, N'Ruben', N'Romero690')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (66, N'Shane', N'Gonzalez257')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (67, N'Marycu', N'Goodman250')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (68, N'Grace', N'Maldonado592')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (69, N'Mary', N'Thompson737')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (70, N'Kurt', N'Miller281')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (71, N'Charles', N'Alvarez935')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (72, N'Rebecca', N'Williams126')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (73, N'Christopher', N'Holloway201')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (74, N'James', N'Stewart442')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (75, N'Christy', N'Johnson381')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (76, N'Lauren', N'Haynes144')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (77, N'Terri', N'Rowe784')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (78, N'Teresa', N'Baker991')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (79, N'Amanda', N'Miller436')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (80, N'Maryam', N'Rodriquez852')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (81, N'Duane', N'Watson884')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (82, N'Debrafe', N'Thornton633')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (83, N'Doris', N'Lewis601')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (84, N'Laurie', N'Porter9')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (85, N'Adam', N'Anderson783')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (86, N'Gwendolyn', N'Stewart16')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (87, N'Bruce', N'Lee689')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (88, N'Williamin', N'Williams474')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (89, N'Ted', N'Jones507')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (90, N'Jeanette', N'Taylor492')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (91, N'Craig', N'Shelton389')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (92, N'Timothy', N'Jordan378')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (93, N'Lucy', N'Cruz658')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (94, N'Ralph', N'Franklin641')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (95, N'Glenni', N'Horton664')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (96, N'Lorraine', N'Salazar27')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (97, N'Robert', N'Brown882')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (98, N'Ella', N'McDonald389')
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (99, N'Ralphos', N'Howard471')
GO
INSERT [dbo].[Login] ([IDLogin], [Login], [Password]) VALUES (100, N'Margareti', N'Roberson998')
SET IDENTITY_INSERT [dbo].[Login] OFF
GO
SET IDENTITY_INSERT [dbo].[Post] ON 

INSERT [dbo].[Post] ([IDPost], [Title]) VALUES (1, N'Директор')
INSERT [dbo].[Post] ([IDPost], [Title]) VALUES (2, N'Работник')
SET IDENTITY_INSERT [dbo].[Post] OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (1, N'1,1"/75 Mark 1', CAST(8667354.00 AS Decimal(10, 2)), 7, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (2, N'1П63 (прицел)', CAST(869593.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (3, N'1П76 (прицел)', CAST(1542522.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (4, N'1П77 (прицел)', CAST(6732087.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (5, N'1П78 (прицел)', CAST(5161416.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (6, N'1ПН93', CAST(5782838.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (7, N'1ПН110', CAST(6939786.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (8, N'2 cm FlaK 30', CAST(1660502.00 AS Decimal(10, 2)), 8, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (9, N'2 cm Flakvierling 38', CAST(1352321.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (10, N'2 cm KwK 30', CAST(4054903.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (11, N'2-дюймовый миномёт', CAST(2303788.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (12, N'2А17', CAST(4476625.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (13, N'2А20', CAST(9760553.00 AS Decimal(10, 2)), 3, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (14, N'2А21', CAST(5810982.00 AS Decimal(10, 2)), 6, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (15, N'2А24', CAST(7993209.00 AS Decimal(10, 2)), 7, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (16, N'2А26', CAST(4491276.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (17, N'2А28', CAST(865699.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (18, N'2А31', CAST(6042835.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (19, N'2А38', CAST(9676314.00 AS Decimal(10, 2)), 8, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (20, N'2А45', CAST(2950320.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (21, N'2А45М', CAST(8222781.00 AS Decimal(10, 2)), 8, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (22, N'2А46', CAST(3479171.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (23, N'2А51', CAST(8718568.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (24, N'2А66', CAST(9380936.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (25, N'2А70', CAST(4619592.00 AS Decimal(10, 2)), 3, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (26, N'2А82', CAST(1341677.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (27, N'2Б9', CAST(2318958.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (28, N'2Б14', CAST(2300708.00 AS Decimal(10, 2)), 6, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (29, N'2Б18', CAST(2858220.00 AS Decimal(10, 2)), 7, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (30, N'2Б23', CAST(8635259.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (31, N'2Б25', CAST(4333946.00 AS Decimal(10, 2)), 2, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (32, N'2К8 «Фаланга»', CAST(1935706.00 AS Decimal(10, 2)), 7, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (33, N'2С12', CAST(4407058.00 AS Decimal(10, 2)), 8, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (34, N'2С15', CAST(6248835.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (35, N'2С25', CAST(4646215.00 AS Decimal(10, 2)), 7, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (36, N'3-дюймовая пушка M5', CAST(9226866.00 AS Decimal(10, 2)), 7, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (37, N'3-дюймовый пехотный миномёт', CAST(8646400.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (38, N'3,7 cm FlaK 18', CAST(1486686.00 AS Decimal(10, 2)), 7, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (39, N'3,7 cm FlaK 43', CAST(1679200.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (40, N'3,7 cm KwK 36', CAST(8474301.00 AS Decimal(10, 2)), 3, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (41, N'3,7 cm SK C/30', CAST(4715215.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (42, N'3"/50 Mark 33', CAST(7762084.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (43, N'3"/70 Mark 6', CAST(5628901.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (44, N'3М6 «Шмель»', CAST(3253850.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (45, N'3М8', CAST(1498940.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (46, N'3М9', CAST(2941351.00 AS Decimal(10, 2)), 3, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (47, N'3Р41', CAST(282448.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (48, N'3Р95 (радиолокационная станция)', CAST(4928202.00 AS Decimal(10, 2)), 8, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (49, N'4,2 cm PaK 41', CAST(224433.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (50, N'4,2-линейный револьвер системы Смита-Вессона', CAST(9074093.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (51, N'4,5-дюймовая гаубица', CAST(6637386.00 AS Decimal(10, 2)), 2, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (52, N'4,5-дюймовая пушка', CAST(5866997.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (53, N'4,5"/45 QF Mark I, III, IV', CAST(1337254.00 AS Decimal(10, 2)), 6, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (54, N'4.5"/45 QF Mark V', CAST(6294422.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (55, N'4" морское орудие Mk IX', CAST(4717496.00 AS Decimal(10, 2)), 6, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (56, N'4" морское орудие Mk V', CAST(6991805.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (57, N'4"/40 QF Mk IV, XII, XXII', CAST(7030416.00 AS Decimal(10, 2)), 2, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (58, N'4TP', CAST(6747275.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (59, N'5 cm KwK 38', CAST(168894.00 AS Decimal(10, 2)), 6, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (60, N'5 cm KwK 39', CAST(8813696.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (61, N'5-дюймовая гаубица', CAST(8495528.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (62, N'5,4-дюймовая гаубица', CAST(8097027.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (63, N'5,5-дюймовая пушка', CAST(6847417.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (64, N'5,25" QF Mark I', CAST(7136269.00 AS Decimal(10, 2)), 3, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (65, N'5,56 mm kbs wz.96 Beryl', CAST(1403014.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (66, N'5"/38 Mark 12', CAST(8421787.00 AS Decimal(10, 2)), 7, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (67, N'5"/54 Mark 42', CAST(9047330.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (68, N'5В28', CAST(4345393.00 AS Decimal(10, 2)), 2, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (69, N'6-дюймовая гаубица образца 1915 года', CAST(4280274.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (70, N'6-дюймовая осадная пушка образца 1904 года', CAST(6972931.00 AS Decimal(10, 2)), 7, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (71, N'6-дюймовая пушка образца 1877 года', CAST(2443964.00 AS Decimal(10, 2)), 7, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (72, N'6-дюймовая пушка Mark XIX', CAST(5099217.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (73, N'6"/35 морская пушка', CAST(8737815.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (74, N'6"/50 BL Mark XXIII', CAST(9685268.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (75, N'6"/50 QF Mark V', CAST(4538841.00 AS Decimal(10, 2)), 2, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (76, N'6Г16', CAST(8082393.00 AS Decimal(10, 2)), 3, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (77, N'6Г30', CAST(1414907.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (78, N'6П62', CAST(7035137.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (79, N'6Х2', CAST(9682818.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (80, N'6Х3', CAST(6026159.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (81, N'6Х4', CAST(9070352.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (82, N'6Х5', CAST(5539660.00 AS Decimal(10, 2)), 6, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (83, N'6Х9', CAST(9893429.00 AS Decimal(10, 2)), 7, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (84, N'7,2-дюймовая гаубица', CAST(7300494.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (85, N'7,5 cm FK 16 nA', CAST(5975744.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (86, N'7,5 cm FK 18', CAST(5926662.00 AS Decimal(10, 2)), 2, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (87, N'7,5 cm FK 38', CAST(6340894.00 AS Decimal(10, 2)), 2, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (88, N'7,5 cm Gebirgsgeschütz 36', CAST(5244507.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (89, N'7,5 cm Infanteriegeschütz 37', CAST(784555.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (90, N'7,5 cm Infanteriegeschütz 42', CAST(9196345.00 AS Decimal(10, 2)), 2, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (91, N'7,5 cm KwK 37', CAST(3524622.00 AS Decimal(10, 2)), 6, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (92, N'7,5 cm KwK 40', CAST(9633048.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (93, N'7,5 cm KwK 42', CAST(1739498.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (94, N'7,5 cm Leichtgeschütz 40', CAST(9492744.00 AS Decimal(10, 2)), 3, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (95, N'7,5 cm PaK 41', CAST(8657789.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (96, N'7,5" морское орудие Mk VI', CAST(5661606.00 AS Decimal(10, 2)), 8, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (97, N'7,62 ITKK 31 VKT', CAST(303622.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (98, N'7.7 cm FK 16', CAST(3466606.00 AS Decimal(10, 2)), 6, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (99, N'7.7 cm FK 96', CAST(3633805.00 AS Decimal(10, 2)), 10, NULL)
GO
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (100, N'7.7 cm FK 96 n.A.', CAST(9574506.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (101, N'7.7 cm Infanteriegeschütz L/20', CAST(985144.00 AS Decimal(10, 2)), 7, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (102, N'7.7 cm Infanteriegeschütz L/27', CAST(6615696.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (103, N'7.7 cm Kanone in Haubitzelafette', CAST(4494443.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (104, N'7.58 cm Minenwerfer', CAST(113320.00 AS Decimal(10, 2)), 8, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (105, N'7.62 cm Infanteriegeschütz L/16.5', CAST(5253406.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (106, N'7TP', CAST(9664824.00 AS Decimal(10, 2)), 3, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (107, N'8 cm FK M.5', CAST(8872956.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (108, N'8-дюймовая гаубица Mark I — V', CAST(5562800.00 AS Decimal(10, 2)), 8, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (109, N'8-дюймовая гаубица Mark VI — VIII', CAST(8340554.00 AS Decimal(10, 2)), 2, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (110, N'8-cm s.Gr.W.34', CAST(3212640.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (111, N'8,8 cm KwK 36', CAST(1737434.00 AS Decimal(10, 2)), 7, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (112, N'8.8 cm L/30 C/08', CAST(8014263.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (113, N'8.8 cm SK L/45', CAST(7996759.00 AS Decimal(10, 2)), 3, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (114, N'8"/35 морская пушка', CAST(3721193.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (115, N'8"/45 морская пушка', CAST(6270809.00 AS Decimal(10, 2)), 2, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (116, N'8"/50 морская пушка', CAST(6070500.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (117, N'8"/55 Mark 71', CAST(566429.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (118, N'9-см бомбомёт типа Г. Р.', CAST(7706493.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (119, N'9,2-дюймовая гаубица', CAST(7874815.00 AS Decimal(10, 2)), 6, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (120, N'9.15 cm leichtes Minenwerfer System Lanz', CAST(413737.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (121, N'9А-91', CAST(7070777.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (122, N'9М27К', CAST(5302074.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (123, N'9М38', CAST(7745891.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (124, N'9М82', CAST(1709735.00 AS Decimal(10, 2)), 2, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (125, N'9М83', CAST(313508.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (126, N'9М112 «Кобра»', CAST(5418871.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (127, N'9П132', CAST(773057.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (128, N'9TP', CAST(3603614.00 AS Decimal(10, 2)), 6, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (129, N'10 cm FH M 14', CAST(9418563.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (130, N'10 cm FH M 99', CAST(9326478.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (131, N'10 cm K 04', CAST(3694315.00 AS Decimal(10, 2)), 7, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (132, N'10 cm K 14', CAST(1780919.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (133, N'10 cm K 17', CAST(957993.00 AS Decimal(10, 2)), 7, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (134, N'10 cm Nebelwerfer 35', CAST(4429078.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (135, N'10 cm Nebelwerfer 40', CAST(4363018.00 AS Decimal(10, 2)), 2, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (136, N'10,5 cm FlaK 38/39', CAST(5295909.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (137, N'10,5 cm Gebirgshaubitze 40', CAST(3596548.00 AS Decimal(10, 2)), 7, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (138, N'10,5 cm leFH 16', CAST(9842239.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (139, N'10,5 cm leFH 18', CAST(2502903.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (140, N'10,5 cm leFH 18/40', CAST(710522.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (141, N'10,5 cm leFH 18M', CAST(1538970.00 AS Decimal(10, 2)), 3, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (142, N'10,5 cm Leichtgeschütz 40', CAST(1314627.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (143, N'10,5 cm Leichtgeschütz 42', CAST(9265255.00 AS Decimal(10, 2)), 8, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (144, N'10,5 cm schwere Kanone 18', CAST(4773278.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (145, N'10,5 cm SK C/33', CAST(5257318.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (146, N'10.5 cm Feldhaubitze 98/09', CAST(9260148.00 AS Decimal(10, 2)), 8, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (147, N'10"/45 морская пушка', CAST(7337357.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (148, N'10"/50 морская пушка', CAST(8135542.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (149, N'10Х', CAST(3385941.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (150, N'10TP', CAST(4035773.00 AS Decimal(10, 2)), 3, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (151, N'12 cm/50 Model 1950', CAST(4542707.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (152, N'12-фунтовая пушка по Грибовалю', CAST(5178130.00 AS Decimal(10, 2)), 2, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (153, N'12,7 cm SKC/34', CAST(6947655.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (154, N'12"/20 морская пушка', CAST(3262284.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (155, N'12"/30 морская пушка', CAST(2278451.00 AS Decimal(10, 2)), 6, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (156, N'12"/35 морская пушка', CAST(6678777.00 AS Decimal(10, 2)), 6, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (157, N'12"/40 морская пушка', CAST(6755971.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (158, N'12"/50 Mark 8', CAST(2390635.00 AS Decimal(10, 2)), 8, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (159, N'12"/52 пушка Обуховского завода', CAST(1875990.00 AS Decimal(10, 2)), 2, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (160, N'13,2 mm Hotchkiss M1929', CAST(7732257.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (161, N'13.5 cm K 09', CAST(9200133.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (162, N'14-дюймовая железнодорожная пушка M1920', CAST(1948261.00 AS Decimal(10, 2)), 3, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (163, N'14"/45 морское орудие', CAST(3049299.00 AS Decimal(10, 2)), 3, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (164, N'14TP', CAST(7393442.00 AS Decimal(10, 2)), 3, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (165, N'15 cm K (E)', CAST(4620715.00 AS Decimal(10, 2)), 8, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (166, N'15 cm Kanone 16', CAST(8570088.00 AS Decimal(10, 2)), 2, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (167, N'15 cm Kanone 18', CAST(9062049.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (168, N'15 cm L/40 Feldkanone i.R.', CAST(6656469.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (169, N'15 cm sFH 02', CAST(1258185.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (170, N'15 cm sFH 13', CAST(4260153.00 AS Decimal(10, 2)), 8, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (171, N'15 cm sFH 18', CAST(773554.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (172, N'15 cm SK Nathan', CAST(569179.00 AS Decimal(10, 2)), 6, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (173, N'15 cm SKC/25', CAST(4063838.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (174, N'15 cm SKC/28', CAST(6684006.00 AS Decimal(10, 2)), 3, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (175, N'15 cm Tbts KC/36', CAST(8115551.00 AS Decimal(10, 2)), 6, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (176, N'15" морское орудие Mk I', CAST(7549579.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (177, N'16"/45 Mark 1', CAST(2691979.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (178, N'16″/45 Mark 6', CAST(9513476.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (179, N'16″/50 Mark 7', CAST(1753359.00 AS Decimal(10, 2)), 9, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (180, N'17 cm K (E)', CAST(4627505.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (181, N'17 cm K.Mrs.Laf', CAST(2592027.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (182, N'17 cm mittlerer Minenwerfer', CAST(9078445.00 AS Decimal(10, 2)), 8, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (183, N'17 cm SK L/40 i.R.L. auf Eisenbahnwagen', CAST(4794116.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (184, N'20 cm leichter Ladungswerfer', CAST(690065.00 AS Decimal(10, 2)), 1, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (185, N'20 ITK 40 VKT', CAST(250874.00 AS Decimal(10, 2)), 3, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (186, N'20 mm modèle F2', CAST(2109291.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (187, N'20 mm/65 Breda Mod. 1935/1939/1940', CAST(8850792.00 AS Decimal(10, 2)), 3, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (188, N'20 mm/70 Scotti Mod. 1939/1941', CAST(2001669.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (189, N'20-мм автоматическая пушка Тип 2', CAST(275404.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (190, N'20-мм зенитная пушка Тип 98', CAST(4703634.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (191, N'20-мм пушки «Эрликон»', CAST(4379183.00 AS Decimal(10, 2)), 10, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (192, N'20,3 cm SKC/34', CAST(2405768.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (193, N'20/25TP', CAST(8376583.00 AS Decimal(10, 2)), 4, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (194, N'21 cm Kanone 38', CAST(7194342.00 AS Decimal(10, 2)), 6, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (195, N'21 cm Kanone 39', CAST(4314636.00 AS Decimal(10, 2)), 8, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (196, N'21 cm Mrs.16', CAST(8787234.00 AS Decimal(10, 2)), 5, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (197, N'21 cm Mrs.18', CAST(7221420.00 AS Decimal(10, 2)), 8, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (198, N'21 cm Nebelwerfer 42', CAST(243078.00 AS Decimal(10, 2)), 8, NULL)
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (199, N'24 cm Haubitze 39', CAST(4159000.00 AS Decimal(10, 2)), 8, NULL)
GO
INSERT [dbo].[Product] ([IDProduct], [Title], [Price], [IDCategory], [Description]) VALUES (200, N'24 cm Kanone 3', CAST(3988594.00 AS Decimal(10, 2)), 9, NULL)
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductList] ON 

INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (1, 13, 8, 48)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (2, 18, 1, 42)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (3, 12, 5, 13)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (4, 1, 3, 49)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (5, 19, 4, 27)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (6, 9, 1, 49)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (7, 4, 10, 4)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (8, 14, 1, 33)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (9, 18, 5, 36)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (10, 19, 7, 31)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (11, 50, 7, 18)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (12, 23, 7, 25)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (13, 43, 4, 10)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (14, 17, 9, 24)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (15, 8, 7, 35)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (16, 47, 10, 34)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (17, 23, 3, 10)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (18, 30, 9, 15)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (19, 24, 4, 41)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (20, 18, 2, 10)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (21, 35, 3, 16)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (22, 49, 10, 29)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (23, 24, 10, 42)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (24, 29, 7, 46)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (25, 49, 2, 21)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (26, 15, 2, 47)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (27, 35, 9, 17)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (28, 50, 3, 9)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (29, 11, 3, 19)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (30, 15, 2, 37)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (31, 11, 1, 42)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (32, 47, 3, 45)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (33, 48, 5, 36)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (34, 4, 7, 39)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (35, 11, 2, 8)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (36, 11, 5, 28)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (37, 12, 7, 18)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (38, 40, 4, 6)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (39, 21, 1, 1)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (40, 48, 9, 40)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (41, 49, 10, 37)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (42, 35, 4, 1)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (43, 16, 6, 44)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (44, 6, 6, 19)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (45, 30, 7, 41)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (46, 6, 3, 43)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (47, 10, 10, 2)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (48, 9, 1, 11)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (49, 10, 9, 48)
INSERT [dbo].[ProductList] ([IDProductList], [IDProduct], [QuantityProd], [IDClient]) VALUES (50, 9, 9, 45)
SET IDENTITY_INSERT [dbo].[ProductList] OFF
GO
SET IDENTITY_INSERT [dbo].[WorkShift] ON 

INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (1, CAST(N'2023-03-23T17:59:28.000' AS DateTime), CAST(N'2023-03-24T17:59:28.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (2, CAST(N'2023-04-07T08:05:30.000' AS DateTime), CAST(N'2023-04-08T08:05:30.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (3, CAST(N'2022-05-04T01:06:30.000' AS DateTime), CAST(N'2022-05-05T01:06:30.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (4, CAST(N'2022-06-08T11:12:40.000' AS DateTime), CAST(N'2022-06-09T11:12:40.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (5, CAST(N'2023-06-26T17:56:50.000' AS DateTime), CAST(N'2023-06-27T17:56:50.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (6, CAST(N'2022-11-09T14:01:45.000' AS DateTime), CAST(N'2022-11-10T14:01:45.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (7, CAST(N'2023-01-16T10:47:58.000' AS DateTime), CAST(N'2023-01-17T10:47:58.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (8, CAST(N'2022-09-20T14:53:13.000' AS DateTime), CAST(N'2022-09-21T14:53:13.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (9, CAST(N'2023-11-16T16:35:34.000' AS DateTime), CAST(N'2023-11-17T16:35:34.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (10, CAST(N'2022-09-22T20:18:28.000' AS DateTime), CAST(N'2022-09-23T20:18:28.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (11, CAST(N'2022-01-25T13:14:26.000' AS DateTime), CAST(N'2022-01-26T13:14:26.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (12, CAST(N'2022-03-10T07:05:14.000' AS DateTime), CAST(N'2022-03-11T07:05:14.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (13, CAST(N'2022-05-11T03:19:41.000' AS DateTime), CAST(N'2022-05-12T03:19:41.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (14, CAST(N'2022-01-19T11:28:57.000' AS DateTime), CAST(N'2022-01-20T11:28:57.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (15, CAST(N'2022-12-18T09:19:51.000' AS DateTime), CAST(N'2022-12-19T09:19:51.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (16, CAST(N'2023-10-06T10:14:19.000' AS DateTime), CAST(N'2023-10-07T10:14:19.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (17, CAST(N'2023-07-22T10:12:04.000' AS DateTime), CAST(N'2023-07-23T10:12:04.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (18, CAST(N'2022-04-28T12:50:16.000' AS DateTime), CAST(N'2022-04-29T12:50:16.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (19, CAST(N'2023-10-17T15:40:58.000' AS DateTime), CAST(N'2023-10-18T15:40:58.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (20, CAST(N'2022-07-19T01:47:14.000' AS DateTime), CAST(N'2022-07-20T01:47:14.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (21, CAST(N'2022-06-12T11:39:39.000' AS DateTime), CAST(N'2022-06-13T11:39:39.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (22, CAST(N'2023-10-05T15:34:42.000' AS DateTime), CAST(N'2023-10-06T15:34:42.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (23, CAST(N'2022-09-18T09:49:42.000' AS DateTime), CAST(N'2022-09-19T09:49:42.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (24, CAST(N'2023-04-08T12:04:26.000' AS DateTime), CAST(N'2023-04-09T12:04:26.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (25, CAST(N'2022-06-21T04:05:25.000' AS DateTime), CAST(N'2022-06-22T04:05:25.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (26, CAST(N'2023-08-21T11:38:00.000' AS DateTime), CAST(N'2023-08-22T11:38:00.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (27, CAST(N'2022-04-07T12:14:58.000' AS DateTime), CAST(N'2022-04-08T12:14:58.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (28, CAST(N'2022-03-17T15:18:29.000' AS DateTime), CAST(N'2022-03-18T15:18:29.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (29, CAST(N'2023-05-01T14:41:17.000' AS DateTime), CAST(N'2023-05-02T14:41:17.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (30, CAST(N'2022-04-17T07:38:48.000' AS DateTime), CAST(N'2022-04-18T07:38:48.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (31, CAST(N'2022-05-03T04:41:42.000' AS DateTime), CAST(N'2022-05-04T04:41:42.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (32, CAST(N'2023-06-05T21:28:53.000' AS DateTime), CAST(N'2023-06-06T21:28:53.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (33, CAST(N'2022-08-19T18:15:52.000' AS DateTime), CAST(N'2022-08-20T18:15:52.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (34, CAST(N'2023-10-02T15:42:14.000' AS DateTime), CAST(N'2023-10-03T15:42:14.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (35, CAST(N'2022-12-17T01:23:36.000' AS DateTime), CAST(N'2022-12-18T01:23:36.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (36, CAST(N'2022-09-21T05:40:45.000' AS DateTime), CAST(N'2022-09-22T05:40:45.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (37, CAST(N'2022-03-17T07:48:32.000' AS DateTime), CAST(N'2022-03-18T07:48:32.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (38, CAST(N'2022-05-13T08:06:35.000' AS DateTime), CAST(N'2022-05-14T08:06:35.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (39, CAST(N'2023-09-03T08:55:24.000' AS DateTime), CAST(N'2023-09-04T08:55:24.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (40, CAST(N'2023-04-13T23:12:57.000' AS DateTime), CAST(N'2023-04-14T23:12:57.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (41, CAST(N'2022-09-02T10:25:55.000' AS DateTime), CAST(N'2022-09-03T10:25:55.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (42, CAST(N'2022-07-28T20:03:48.000' AS DateTime), CAST(N'2022-07-29T20:03:48.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (43, CAST(N'2022-06-19T06:04:40.000' AS DateTime), CAST(N'2022-06-20T06:04:40.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (44, CAST(N'2023-01-09T15:40:01.000' AS DateTime), CAST(N'2023-01-10T15:40:01.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (45, CAST(N'2022-05-22T17:54:49.000' AS DateTime), CAST(N'2022-05-23T17:54:49.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (46, CAST(N'2023-02-18T10:37:26.000' AS DateTime), CAST(N'2023-02-19T10:37:26.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (47, CAST(N'2023-01-20T21:48:55.000' AS DateTime), CAST(N'2023-01-21T21:48:55.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (48, CAST(N'2022-12-08T04:07:46.000' AS DateTime), CAST(N'2022-12-09T04:07:46.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (49, CAST(N'2023-01-28T03:51:12.000' AS DateTime), CAST(N'2023-01-29T03:51:12.000' AS DateTime))
INSERT [dbo].[WorkShift] ([IDWorkShift], [StartTime], [EndTime]) VALUES (50, CAST(N'2022-08-13T12:11:47.000' AS DateTime), CAST(N'2022-08-14T12:11:47.000' AS DateTime))
SET IDENTITY_INSERT [dbo].[WorkShift] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UC_Login]    Script Date: 29.09.2023 2:25:53 ******/
ALTER TABLE [dbo].[Login] ADD  CONSTRAINT [UC_Login] UNIQUE NONCLUSTERED 
(
	[Login] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Certificate] ADD  CONSTRAINT [DF_Certificate_Description]  DEFAULT (NULL) FOR [Description]
GO
ALTER TABLE [dbo].[Emloyee] ADD  CONSTRAINT [DF_Emloyee_IDPost]  DEFAULT ((2)) FOR [IDPost]
GO
ALTER TABLE [dbo].[Product] ADD  CONSTRAINT [DF_Product_IDCategory]  DEFAULT ((1)) FOR [IDCategory]
GO
ALTER TABLE [dbo].[Product] ADD  CONSTRAINT [DF_Product_Description]  DEFAULT (NULL) FOR [Description]
GO
ALTER TABLE [dbo].[ProductList] ADD  CONSTRAINT [DF_ProductList_QuantityProd]  DEFAULT ((1)) FOR [QuantityProd]
GO
ALTER TABLE [dbo].[Category]  WITH CHECK ADD  CONSTRAINT [FK_Category_Certificate] FOREIGN KEY([IDCertificate])
REFERENCES [dbo].[Certificate] ([IDCertificate])
GO
ALTER TABLE [dbo].[Category] CHECK CONSTRAINT [FK_Category_Certificate]
GO
ALTER TABLE [dbo].[CertificateClient]  WITH CHECK ADD  CONSTRAINT [FK_CertificateClient_Certificate] FOREIGN KEY([IDCertificate])
REFERENCES [dbo].[Certificate] ([IDCertificate])
GO
ALTER TABLE [dbo].[CertificateClient] CHECK CONSTRAINT [FK_CertificateClient_Certificate]
GO
ALTER TABLE [dbo].[CertificateClient]  WITH CHECK ADD  CONSTRAINT [FK_CertificateClient_Client] FOREIGN KEY([IDClient])
REFERENCES [dbo].[Client] ([IDClient])
GO
ALTER TABLE [dbo].[CertificateClient] CHECK CONSTRAINT [FK_CertificateClient_Client]
GO
ALTER TABLE [dbo].[Check]  WITH CHECK ADD  CONSTRAINT [FK_Check_Emloyee] FOREIGN KEY([IDEmployee])
REFERENCES [dbo].[Emloyee] ([IDEmployee])
GO
ALTER TABLE [dbo].[Check] CHECK CONSTRAINT [FK_Check_Emloyee]
GO
ALTER TABLE [dbo].[Check]  WITH CHECK ADD  CONSTRAINT [FK_Check_ProductList] FOREIGN KEY([IDProductList])
REFERENCES [dbo].[ProductList] ([IDProductList])
GO
ALTER TABLE [dbo].[Check] CHECK CONSTRAINT [FK_Check_ProductList]
GO
ALTER TABLE [dbo].[Client]  WITH CHECK ADD  CONSTRAINT [FK_Client_Gender] FOREIGN KEY([IDGender])
REFERENCES [dbo].[Gender] ([IDGender])
GO
ALTER TABLE [dbo].[Client] CHECK CONSTRAINT [FK_Client_Gender]
GO
ALTER TABLE [dbo].[Client]  WITH CHECK ADD  CONSTRAINT [FK_Client_Login] FOREIGN KEY([IDLogin])
REFERENCES [dbo].[Login] ([IDLogin])
GO
ALTER TABLE [dbo].[Client] CHECK CONSTRAINT [FK_Client_Login]
GO
ALTER TABLE [dbo].[Emloyee]  WITH CHECK ADD  CONSTRAINT [FK_Emloyee_Login] FOREIGN KEY([IDLogin])
REFERENCES [dbo].[Login] ([IDLogin])
GO
ALTER TABLE [dbo].[Emloyee] CHECK CONSTRAINT [FK_Emloyee_Login]
GO
ALTER TABLE [dbo].[Emloyee]  WITH CHECK ADD  CONSTRAINT [FK_Emloyee_Post] FOREIGN KEY([IDPost])
REFERENCES [dbo].[Post] ([IDPost])
GO
ALTER TABLE [dbo].[Emloyee] CHECK CONSTRAINT [FK_Emloyee_Post]
GO
ALTER TABLE [dbo].[EmloyeeWorkShift]  WITH CHECK ADD  CONSTRAINT [FK_EmloyerWorkShift_Emloyee] FOREIGN KEY([IDEmployee])
REFERENCES [dbo].[Emloyee] ([IDEmployee])
GO
ALTER TABLE [dbo].[EmloyeeWorkShift] CHECK CONSTRAINT [FK_EmloyerWorkShift_Emloyee]
GO
ALTER TABLE [dbo].[EmloyeeWorkShift]  WITH CHECK ADD  CONSTRAINT [FK_EmloyerWorkShift_WorkShift] FOREIGN KEY([IDWorkshift])
REFERENCES [dbo].[WorkShift] ([IDWorkShift])
GO
ALTER TABLE [dbo].[EmloyeeWorkShift] CHECK CONSTRAINT [FK_EmloyerWorkShift_WorkShift]
GO
ALTER TABLE [dbo].[Product]  WITH CHECK ADD  CONSTRAINT [FK_Product_Category] FOREIGN KEY([IDCategory])
REFERENCES [dbo].[Category] ([IDCategory])
GO
ALTER TABLE [dbo].[Product] CHECK CONSTRAINT [FK_Product_Category]
GO
ALTER TABLE [dbo].[ProductList]  WITH CHECK ADD  CONSTRAINT [FK_ProductList_Client] FOREIGN KEY([IDClient])
REFERENCES [dbo].[Client] ([IDClient])
GO
ALTER TABLE [dbo].[ProductList] CHECK CONSTRAINT [FK_ProductList_Client]
GO
ALTER TABLE [dbo].[ProductList]  WITH CHECK ADD  CONSTRAINT [FK_ProductList_Product] FOREIGN KEY([IDProduct])
REFERENCES [dbo].[Product] ([IDProduct])
GO
ALTER TABLE [dbo].[ProductList] CHECK CONSTRAINT [FK_ProductList_Product]
GO
