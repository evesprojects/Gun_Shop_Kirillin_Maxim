<h1>Магазин оружия <h1>

**Краткое описание:**

Данный проект созлан с целью автоматизировать продажу и учёт товаров в оружейном магазине

приложение включит в себя:

1)	Процесс учёта амуниции на складе
2)	Учёт сотрудников
3)	Процесс продажи амуниции третьим лицам
4)	Проверка лицензирования лиц
5)	Разбиение амуниции на категории


Более полное описание можно посмотреть в техническом задании: ![TЗ](https://gitlab.com/evesprojects/Gun_Shop_Kirillin_Maxim/-/blob/main/%D0%A2%D0%97.docx?ref_type=heads) 

ERD Диограмма БД:

![ERD](https://github.com/EVETuesday/CoffeeHouse/assets/114149936/3374a237-7a56-4ce5-b480-125c58f146a4)


UseCase Диограмма:


![UseCaseUML](https://github.com/EVETuesday/CoffeeHouse/assets/114149936/260f1203-5fd7-4b30-aa5c-8ec8325cc603)


Дизайн:


![Дизайн](https://gitlab.com/evesprojects/Gun_Shop_Kirillin_Maxim/-/raw/main/1OtherFiles1/Res/Photo/Design.png?ref_type=heads)

https://www.figma.com/file/ZWdIlnJZZ0vCAcNdfgD91w/Untitled?type=design&node-id=0%3A1&mode=design&t=SzF5Ubpsm3ShvzDS-1



Физическая схема БД:

![схема](https://gitlab.com/evesprojects/Gun_Shop_Kirillin_Maxim/-/raw/main/1OtherFiles1/Res/Photo/PhysicalERD/AllDataBase.png?ref_type=heads)

![схема1](https://gitlab.com/evesprojects/Gun_Shop_Kirillin_Maxim/-/raw/main/1OtherFiles1/Res/Photo/PhysicalERD/1.png?ref_type=heads)

![схема2](https://gitlab.com/evesprojects/Gun_Shop_Kirillin_Maxim/-/raw/main/1OtherFiles1/Res/Photo/PhysicalERD/2.png?ref_type=heads)

![схема3](https://gitlab.com/evesprojects/Gun_Shop_Kirillin_Maxim/-/raw/main/1OtherFiles1/Res/Photo/PhysicalERD/3.png?ref_type=heads)


Данные:

![Data](https://gitlab.com/evesprojects/Gun_Shop_Kirillin_Maxim/-/raw/main/1OtherFiles1/Res/Photo/PhysicalERD/SelectFromAll.png?ref_type=heads)




Диограмма   Последовательности:

![DiogramOfTheSequence](https://gitlab.com/evesprojects/Gun_Shop_Kirillin_Maxim/-/raw/main/1OtherFiles1/Res/Photo/DiogramOfTheSequence.png?ref_type=heads)




Диограмма классов:

![DiogramOfTheClasses](https://gitlab.com/evesprojects/Gun_Shop_Kirillin_Maxim/-/raw/main/1OtherFiles1/Res/Photo/DiogramOfTheClasses.png?ref_type=heads)



Диограмма активности:

![DiogramOfTheActivity](https://gitlab.com/evesprojects/Gun_Shop_Kirillin_Maxim/-/raw/main/1OtherFiles1/Res/Photo/DiogramOfTheActivity.png?ref_type=heads)
