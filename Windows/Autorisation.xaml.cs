﻿using ShopGun.DB;
using ShopGun.Windows.Client;
using ShopGun.Windows.Empliyee;
using ShopGun.Windows.Employee;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static ShopGun.ClassHelper.ClassHelperC;

namespace ShopGun.Windows
{
    /// <summary>
    /// Логика взаимодействия для Autorisation.xaml
    /// </summary>
    public partial class Autorisation : Window
    {
        public Autorisation()
        {
            InitializeComponent();

        }

        private void btnGo_Click(object sender, RoutedEventArgs e)
        {
            Login login= ContextC.Login.ToList().Where(i => i.Login1 == tbLogin.Text && i.Password==tbPassword.Text).FirstOrDefault();
            if ((login==null))
            {
                MessageBox.Show("Не верные логин или пароль", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            DB.Client client = ContextC.Client.ToList().Where(i=>i.Login==login).FirstOrDefault();
            if (client!=null)
            {
                ClientMenu clientMenu = new ClientMenu();
                clientMenu.Show();
                Close();
            }
            else
            {
                EmployeeMenu employeeMenu = new EmployeeMenu(login);
                employeeMenu.Show();
                Close();
            }
            
        }

        private void btnRegistration_Click(object sender, RoutedEventArgs e)
        {
            Registration registration = new Registration();
            registration.Show();
            Close();
        }
    }
}
