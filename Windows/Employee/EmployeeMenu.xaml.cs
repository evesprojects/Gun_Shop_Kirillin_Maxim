﻿using ShopGun.DB;
using ShopGun.Windows.Transaction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static ShopGun.ClassHelper.ClassHelperC;

namespace ShopGun.Windows.Employee
{
    /// <summary>
    /// Логика взаимодействия для EmployeeMenu.xaml
    /// </summary>
    public partial class EmployeeMenu : Window
    {
        ShopGun.DB.Emloyee emloyee;
        public EmployeeMenu(Login login)
        {
            InitializeComponent();
            emloyee = ContextC.Emloyee.ToList().Where(i=>i.IDLogin == login.IDLogin).FirstOrDefault();
            tbNameEmployee.Text=emloyee.FirstName+" "+emloyee.LastName+" "+emloyee.Patronymic+" | "+emloyee.Post.Title;
            EmloyeeActive = emloyee;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ProductListToCart productListToCart = new ProductListToCart();
            productListToCart.Show();
            Close();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Autorisation autorisation = new Autorisation();
            autorisation.Show();
            Close();
            EmloyeeActive = null;
        }
    }
}
