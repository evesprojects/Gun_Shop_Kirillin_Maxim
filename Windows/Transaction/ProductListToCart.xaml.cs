﻿using ShopGun.DB;
using ShopGun.Windows.Employee;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static ShopGun.ClassHelper.ClassHelperC;

namespace ShopGun.Windows.Transaction
{
    /// <summary>
    /// Логика взаимодействия для ProductListToCart.xaml
    /// </summary>
    public partial class ProductListToCart : Window
    {
        bool orderG = true;
        public ProductListToCart()
        {
            InitializeComponent();
            GetProducts();
            GetTotalCost();

        }
        void GetProducts()
        {
            lvProductList.ItemsSource = products;
        }
        void GetProducts(bool order)
        {
            switch (cmbSort.SelectedIndex)
            {
                case 0:
                    if (order)
                    {
                        lvProductList.ItemsSource = products.OrderBy(i => i.IDProduct);
                    }
                    else
                    {
                        lvProductList.ItemsSource = products.OrderByDescending(i => i.IDProduct);
                    }
                    break;
                case 1:
                    if (order)
                    {
                        lvProductList.ItemsSource = products.OrderBy(i => i.Title);
                    }
                    else
                    {
                        lvProductList.ItemsSource = products.OrderByDescending(i => i.Title);
                    }
                    break;
                case 2:
                    if (order)
                    {
                        lvProductList.ItemsSource = products.OrderByDescending(i => i.Price);
                    }
                    else
                    {
                        lvProductList.ItemsSource = products.OrderBy(i => i.Price);
                    }
                    break;
                case 3:
                    if (order)
                    {
                        lvProductList.ItemsSource = products.OrderBy(i => i.Category.Title);
                    }
                    else
                    {
                        lvProductList.ItemsSource = products.OrderByDescending(i => i.Category.Title);
                    }
                    break;
                default:
                    break;
            }
            
            
        }
        void GetTotalCost()
        {
            lblTotalCost.Content = 0;
            foreach (var item in cartProduct)
            {
                for (int i = 0; i < item.Quantity; i++)
                {
                    lblTotalCost.Content = (Convert.ToDouble(lblTotalCost.Content) + Convert.ToDouble(item.Price)).ToString();
                }                
            }
        }

        private void Button_ClickAddProduct(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            Product product = button.DataContext as Product;
            if (button.Content.ToString()=="+")
            {
                product.Quantity++;
                int i = products.IndexOf(product);
                products.Remove(product);
                products.Insert(i, product);
            }
            else if(product.Quantity>0)
            {
                product.Quantity--;
                int i = products.IndexOf(product);
                products.Remove(product);
                products.Insert(i, product);
            }
            if (cartProduct.Where(i=>i.IDProduct==product.IDProduct).FirstOrDefault()==null || cartProduct.Count==0)
            {
                cartProduct.Add(product);
            }
            if (cartProduct.Where(i => i.IDProduct == product.IDProduct).FirstOrDefault() != null && cartProduct.Where(i => i.IDProduct == product.IDProduct).FirstOrDefault().Quantity<=0)
            {
                cartProduct.Remove(product);
            }
            GetTotalCost();
            GetProducts(orderG);
        }

        private void tblSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            products = new ObservableCollection<Product>(ContextC.Product.ToList().Where(i => i.SearchName.ToLower().Contains(tblSearch.Text.ToLower())));
            GetProducts();
        }

        private void OrderBy_Click(object sender, RoutedEventArgs e)
        {Button button = sender as Button;
            if (button.Content.ToString()=="↑")
            {
                GetProducts(true);
                orderG = true;
            }
            else
            {
                GetProducts(false);
                orderG = false;
            }
        }

        private void btnBy_Click(object sender, RoutedEventArgs e)
        {
            if (cartProduct.Count<=0)
            {
                MessageBox.Show("Товары не выбраны", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Cart cart = new Cart();
            cart.Show();
            Close();
        }
        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            EmployeeMenu employeeMenu = new EmployeeMenu(EmloyeeActive.Login);
            employeeMenu.Show();
            Close();
        }
    }
}
