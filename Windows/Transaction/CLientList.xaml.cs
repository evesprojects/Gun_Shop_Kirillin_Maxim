﻿using ShopGun.DB;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static ShopGun.ClassHelper.ClassHelperC;

namespace ShopGun.Windows.Transaction
{
    /// <summary>
    /// Логика взаимодействия для CLientList.xaml
    /// </summary>
    public partial class CLientList : Window
    {
        Cart cart;
        public CLientList(Cart cart1)
        {
            InitializeComponent();
            GetClients();
            cart = cart1;
        }
        void GetClients()
        {
            lvClientList.ItemsSource = clients;
        }

        void GetClients(bool order)
        {
            switch (cmbSort.SelectedIndex)
            {
                case 0:
                    if (order)
                    {
                        lvClientList.ItemsSource = clients.OrderBy(i => i.IDClient);
                    }
                    else
                    {
                        lvClientList.ItemsSource = clients.OrderByDescending(i => i.IDClient);
                    }
                    break;
                case 1:
                    if (order)
                    {
                        lvClientList.ItemsSource = clients.OrderBy(i => i.FirstName);
                    }
                    else
                    {
                        lvClientList.ItemsSource = clients.OrderByDescending(i => i.FirstName);
                    }
                    break;
                case 2:
                    if (order)
                    {
                        lvClientList.ItemsSource = clients.OrderBy(i => i.LastName);
                    }
                    else
                    {
                        lvClientList.ItemsSource = clients.OrderByDescending(i => i.LastName);
                    }
                    break;
                case 3:
                    if (order)
                    {
                        lvClientList.ItemsSource = clients.OrderBy(i => i.Patronymic);
                    }
                    else
                    {
                        lvClientList.ItemsSource = clients.OrderByDescending(i => i.Patronymic);
                    }
                    break;
                case 4:
                    if (order)
                    {
                        lvClientList.ItemsSource = clients.OrderBy(i => i.Phone);
                    }
                    else
                    {
                        lvClientList.ItemsSource = clients.OrderByDescending(i => i.Phone);
                    }
                    break;
                default:
                    break;
            }


        }

        private void tblSearch_TextChanged(object sender, TextChangedEventArgs e)
        {
            clients = new ObservableCollection<DB.Client>(ContextC.Client.ToList().Where(i => i.FirstName.ToLower().Contains(tblSearch.Text.ToLower()) || i.LastName.ToLower().Contains(tblSearch.Text.ToLower()) || i.Patronymic.ToLower().Contains(tblSearch.Text.ToLower()) || i.Phone.ToLower().Contains(tblSearch.Text.ToLower())));
            GetClients();
        }

        private void OrderBy_Click(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button.Content.ToString() == "↑")
            {
                GetClients(true);
            }
            else
            {
                GetClients(false);
            }
        }

        private void btnBy_Click(object sender, RoutedEventArgs e)
        {
            if (lvClientList.SelectedItem!=null)
            {
                ClientActive = lvClientList.SelectedItem as DB.Client;
                List<DB.Client> temp = new List<DB.Client>();
                temp.Add(ClientActive);
                cart.lvClient.ItemsSource = temp;
                Close();
            }
            else
            {
                MessageBox.Show("Клиент не выбран");
            }            
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
