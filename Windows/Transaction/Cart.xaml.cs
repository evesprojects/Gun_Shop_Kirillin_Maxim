﻿using ShopGun.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using static ShopGun.ClassHelper.ClassHelperC;

namespace ShopGun.Windows.Transaction
{
    /// <summary>
    /// Логика взаимодействия для Cart.xaml
    /// </summary>
    public partial class Cart : Window
    {
        public Cart()
        {
            InitializeComponent();
            GetProducts();
            GetTotalCost(); 
        }
        void GetProducts()
        {
            lvProductList.ItemsSource = cartProduct;
        }
        private void Button_ClickAddProduct(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            Product product = button.DataContext as Product;
            if (button.Content.ToString() == "+")
            {
                product.Quantity++;
                int i = cartProduct.IndexOf(product);
                cartProduct.Remove(product);
                cartProduct.Insert(i, product);
            }
            else if (product.Quantity > 0)
            {
                product.Quantity--;
                int i = cartProduct.IndexOf(product);
                cartProduct.Remove(product);
                cartProduct.Insert(i, product);
            }
            if (cartProduct.Where(i => i.IDProduct == product.IDProduct).FirstOrDefault() == null || cartProduct.Count == 0)
            {
                cartProduct.Add(product);
            }
            if (cartProduct.Where(i => i.IDProduct == product.IDProduct).FirstOrDefault() != null && cartProduct.Where(i => i.IDProduct == product.IDProduct).FirstOrDefault().Quantity <= 0)
            {
                cartProduct.Remove(product);
            }
            GetTotalCost();
            if (cartProduct.Count==0)
            {
                ProductListToCart productListToCart = new ProductListToCart();
                productListToCart.Show();
                Close();
            }
        }
        void GetTotalCost()
        {
            lblTotalCost.Content = 0;
            foreach (var item in cartProduct)
            {
                for (int i = 0; i < item.Quantity; i++)
                {
                    lblTotalCost.Content = (Convert.ToDouble(lblTotalCost.Content) + Convert.ToDouble(item.Price)).ToString();
                }
            }
        }

        private void btnbuy_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (ClientActive==null)
                {
                    MessageBox.Show("Клиент не выбран", "Error", MessageBoxButton.OK,MessageBoxImage.Error);
                    return;
                }
                Check check = new Check();
                check.Emloyee = EmloyeeActive;
                check.DateOfSale=DateTime.Now;
                ContextC.Check.Add(check);
                foreach (var item in cartProduct)
                {
                    ProductList productList = new ProductList();
                    productList.Product=item;
                    productList.Check = check;
                    productList.Client = ClientActive;
                    productList.QuantityProd = item.Quantity;
                    ContextC.ProductList.Add(productList);
                }
                ContextC.SaveChanges();
                MessageBox.Show("Покупка произошла успешно");
                ProductListToCart productListToCart = new ProductListToCart();
                productListToCart.Show();
                Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnClientChose_Click(object sender, RoutedEventArgs e)
        {
            CLientList cLientList = new CLientList(this);
            cLientList.Show();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            ProductListToCart productListToCart = new ProductListToCart();
            productListToCart.Show();
            Close();
        }
    }
}
