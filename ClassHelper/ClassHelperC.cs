﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ShopGun.DB;

namespace ShopGun.ClassHelper
{
    static public class ClassHelperC
    {
        static public Entities2 ContextC { get; } = new Entities2();
        public static ObservableCollection<DB.Product> products = new ObservableCollection<DB.Product>(ContextC.Product.ToList());
        public static ObservableCollection<DB.Client> clients = new ObservableCollection<DB.Client>(ContextC.Client.ToList());
        public static ObservableCollection<DB.Product> cartProduct = new ObservableCollection<DB.Product>();

        public static Emloyee EmloyeeActive { get; set; }
        public static Client ClientActive { get; set; }
    }
}
